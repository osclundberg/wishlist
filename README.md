### Behövlista
* [Utbildningskit solceller](http://www.kjell.com/sortiment/el/elektronik/elektroniklab/utbildningskit-solar-p36304), 200:-
* Skruvdragare 
* [Papperskorg biltema](http://www.biltema.se/sv/Kontor---Teknik/Kontor/Forvaring/Papperskorg-2000020046/) artikelnr: 28-346 	50:-


### Önskelista
* [Whiskystenar](http://www.teknikmagasinet.se/produkter/maskerad-o-party/fest-o-party/whiskeystenar), 139:-
* [Harmon Karmen Onyx](http://www.harmankardon.com/content?ContentID=onyx#) högtalare, 3000:-
* Brödrost, någon billig typ 150:-
* [USB kabel till arduino](http://www.kjell.com/sortiment/el/elektronik/mikrokontroller/arduino/usb-till-seriell-p87898), 99:-
* Amazon Kindle Paperwhite, 1500:-
* [LIFX lampa](http://lifx.co/), 750:-
* [Airtame](https://airtame.com/) trådlös video, 1000:-

### Kanske
* [SCiO](https://www.consumerphysics.com/myscio/order) (cool sensor), 2500:-
* Ljudisolerade hörlurar
* Elektroniskt piano
* Skrivbordslampa